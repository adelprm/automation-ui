<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Address</name>
   <tag></tag>
   <elementGuidId>7afd9107-1e25-4ef2-b494-3bf24ccd92c4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#stickyNavContainer</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='stickyNavContainer']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>container</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>stickyNavContainer</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            
      Address
                  
        
      Marrickville Metro Shopping Centre Shop 9 20 Smidmore Street Marrickville NSW 2204
    
  
            
        
      Contact Service NSW
    
  
      
        
      Use ctrl + scroll to zoom the map
          Marrickville Service Centre 
          
          
            Marrickville Metro Shopping Centre
            Shop 9
            
            20 Smidmore Street
            
            Marrickville NSW 2204
          
          Closed for Australia Day
          Directions
        MapTerrainSatelliteLabelsMap DataMap data ©2021Terms of UseReport a map errorMap data ©2021Map DataMap data ©2021

    
  
            
      Nearest locations
        
      Haymarket Service CentreRozelle Bay Maritime Service CentreBotany Service Centre
    
  

    
                
      Opening hours
          
  
      Opening hours for
      
      
        this week
        next week
      
      

  
          
        
          
            
              Weekday
              Date
              Hours
            
          
          
                      
              Tuesday
              26 January
              
                                Closed for Australia Day
              
            
                      
              Wednesday
              27 January
              
                                
                                  
                                        9:00am to 5:00pm
                  
                                
                                                  
              
            
                      
              Thursday
              28 January
              
                                
                                  
                                        9:00am to 5:00pm
                  
                                
                                                  
              
            
                      
              Friday
              29 January
              
                                
                                  
                                        9:00am to 5:00pm
                  
                                
                                                  
              
            
                      
              Saturday
              30 January
              
                                
                                  
                                        8:30am to 12:30pm
                  
                                
                                                  
              
            
                      
              Sunday
              31 January
              
                                Closed
              
            
                    
        
      
          
        
          
            
              Weekday
              Date
              Hours
            
          
          
                      
              Monday
              1 February
              
                                
                                  
                                        9:00am to 5:00pm
                  
                                
                                                  
              
            
                      
              Tuesday
              2 February
              
                                
                                  
                                        9:00am to 5:00pm
                  
                                
                                                  
              
            
                      
              Wednesday
              3 February
              
                                
                                  
                                        9:00am to 5:00pm
                  
                                
                                                  
              
            
                      
              Thursday
              4 February
              
                                
                                  
                                        9:00am to 5:00pm
                  
                                
                                                  
              
            
                      
              Friday
              5 February
              
                                
                                  
                                        9:00am to 5:00pm
                  
                                
                                                  
              
            
                      
              Saturday
              6 February
              
                                
                                  
                                        8:30am to 12:30pm
                  
                                
                                                  
              
            
                      
              Sunday
              7 February
              
                                Closed
              
            
                    
        
      
      




              
      Busy times
        
      
      
      Due to COVID-19 physical distancing requirements, you may need to wait outside before entering the service centre. The average wait times shown below do not include any outside waiting time.
    
    
    Select a column below to find out average wait times at this location on
    
      
                  Monday
                  Tuesday
                  Wednesday
                  Thursday
                  Friday
                  Saturday
                  Sunday
              
    
  
  
        
      
              
          
            9:00am
            9am
          
          
            10:00am
            10am
          
          
          
            9:00am to 10:00am: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            10:00am
            10am
          
          
            11:00am
            11am
          
          
          
            10:00am to 11:00am: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            11:00am
            11am
          
          
            12:00pm
            12pm
          
          
          
            11:00am to 12:00pm: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            12:00pm
            12pm
          
          
            1:00pm
            1pm
          
          
          
            12:00pm to 1:00pm: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            1:00pm
            1pm
          
          
            2:00pm
            2pm
          
          
          
            1:00pm to 2:00pm: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            2:00pm
            2pm
          
          
            3:00pm
            3pm
          
          
          
            2:00pm to 3:00pm: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            3:00pm
            3pm
          
          
            4:00pm
            4pm
          
          
          
            3:00pm to 4:00pm: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            4:00pm
            4pm
          
          
            5:00pm
            5pm
          
          
          
            4:00pm to 5:00pm: Quiet
            Average wait: 0-5 minutes
          
        
                  
    
        
      
              
          
            9:00am
            9am
          
          
            10:00am
            10am
          
          
          
            9:00am to 10:00am: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            10:00am
            10am
          
          
            11:00am
            11am
          
          
          
            10:00am to 11:00am: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            11:00am
            11am
          
          
            12:00pm
            12pm
          
          
          
            11:00am to 12:00pm: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            12:00pm
            12pm
          
          
            1:00pm
            1pm
          
          
          
            12:00pm to 1:00pm: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            1:00pm
            1pm
          
          
            2:00pm
            2pm
          
          
          
            1:00pm to 2:00pm: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            2:00pm
            2pm
          
          
            3:00pm
            3pm
          
          
          
            2:00pm to 3:00pm: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            3:00pm
            3pm
          
          
            4:00pm
            4pm
          
          
          
            3:00pm to 4:00pm: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            4:00pm
            4pm
          
          
            5:00pm
            5pm
          
          
          
            4:00pm to 5:00pm: Quiet
            Average wait: 0-5 minutes
          
        
                  
    
        
      
              
          
            9:00am
            9am
          
          
            10:00am
            10am
          
          
          
            9:00am to 10:00am: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            10:00am
            10am
          
          
            11:00am
            11am
          
          
          
            10:00am to 11:00am: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            11:00am
            11am
          
          
            12:00pm
            12pm
          
          
          
            11:00am to 12:00pm: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            12:00pm
            12pm
          
          
            1:00pm
            1pm
          
          
          
            12:00pm to 1:00pm: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            1:00pm
            1pm
          
          
            2:00pm
            2pm
          
          
          
            1:00pm to 2:00pm: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            2:00pm
            2pm
          
          
            3:00pm
            3pm
          
          
          
            2:00pm to 3:00pm: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            3:00pm
            3pm
          
          
            4:00pm
            4pm
          
          
          
            3:00pm to 4:00pm: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            4:00pm
            4pm
          
          
            5:00pm
            5pm
          
          
          
            4:00pm to 5:00pm: Quiet
            Average wait: 0-5 minutes
          
        
                  
    
        
      
              
          
            9:00am
            9am
          
          
            10:00am
            10am
          
          
          
            9:00am to 10:00am: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            10:00am
            10am
          
          
            11:00am
            11am
          
          
          
            10:00am to 11:00am: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            11:00am
            11am
          
          
            12:00pm
            12pm
          
          
          
            11:00am to 12:00pm: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            12:00pm
            12pm
          
          
            1:00pm
            1pm
          
          
          
            12:00pm to 1:00pm: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            1:00pm
            1pm
          
          
            2:00pm
            2pm
          
          
          
            1:00pm to 2:00pm: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            2:00pm
            2pm
          
          
            3:00pm
            3pm
          
          
          
            2:00pm to 3:00pm: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            3:00pm
            3pm
          
          
            4:00pm
            4pm
          
          
          
            3:00pm to 4:00pm: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            4:00pm
            4pm
          
          
            5:00pm
            5pm
          
          
          
            4:00pm to 5:00pm: Quiet
            Average wait: 0-5 minutes
          
        
                  
    
        
      
              
          
            9:00am
            9am
          
          
            10:00am
            10am
          
          
          
            9:00am to 10:00am: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            10:00am
            10am
          
          
            11:00am
            11am
          
          
          
            10:00am to 11:00am: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            11:00am
            11am
          
          
            12:00pm
            12pm
          
          
          
            11:00am to 12:00pm: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            12:00pm
            12pm
          
          
            1:00pm
            1pm
          
          
          
            12:00pm to 1:00pm: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            1:00pm
            1pm
          
          
            2:00pm
            2pm
          
          
          
            1:00pm to 2:00pm: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            2:00pm
            2pm
          
          
            3:00pm
            3pm
          
          
          
            2:00pm to 3:00pm: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            3:00pm
            3pm
          
          
            4:00pm
            4pm
          
          
          
            3:00pm to 4:00pm: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            4:00pm
            4pm
          
          
            5:00pm
            5pm
          
          
          
            4:00pm to 5:00pm: Quiet
            Average wait: 0-5 minutes
          
        
                  
    
        
      
              
          
            8:00am
            8am
          
          
            9:00am
            9am
          
          
          
            8:00am to 9:00am: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            9:00am
            9am
          
          
            10:00am
            10am
          
          
          
            9:00am to 10:00am: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            10:00am
            10am
          
          
            11:00am
            11am
          
          
          
            10:00am to 11:00am: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            11:00am
            11am
          
          
            12:00pm
            12pm
          
          
          
            11:00am to 12:00pm: Quiet
            Average wait: 0-5 minutes
          
        
              
          
            12:00pm
            12pm
          
          
            1:00pm
            1pm
          
          
          
            12:00pm to 1:00pm: Quiet
            Average wait: 0-5 minutes
          
        
                  
    
        
      
                    Closed today – choose another day
            
    
      


    
  

    
                
      Services
                  
        
          We're the NSW government agency where you can apply for a licence, get a permit, register a birth, pay most fines and more.
  


    Browse our services to see what we offer and also find out if you can get what you need online.
  

    
  

    
                
      Plan ahead
                  
        
          Prepare your documents

Save time by getting your paperwork and supporting documents ready before you arrive.

Search our website for the service you’re looking for. Find out what you’ll need to bring or if you can complete your task online.
  


    New photocards will be posted

When you apply to get, renew or replace a photocard at a service centre or registry, it will be posted to you at no extra cost, within 10 business days.

This applies to:


	NSW Driver Licences
	Mobility Parking Scheme cards
	NSW Photo Cards
	Firearms, Security, and Commercial Agents and Private Inquiry Agents licences.

  


    Parking

Shopping centre parking bays.
  


Driver test parking

Use the Murray Street entry and park in the allocated driving test area. The allocated testing bays are located on the right-hand side of the ramp opposite Kmart Auto.
    
  
            
      Payment methods
        
          
	Cash
	Cheque (personal or bank)
	Credit or debit card
	Money order


A merchant fee may apply when paying by credit or debit card.
  

    
  
            
        
      
Accessibility facilities

      Auslan video remote interpreting
      Hearing loop
      Remote live captioning
      Wheelchair access
  


    
  

    
        
  Back to top


    PrintShare
  
  


  


  
  
  


  
  


  



  </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;stickyNavContainer&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='stickyNavContainer']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='block-content']/article/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Plan ahead'])[1]/following::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Services'])[1]/following::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//article/div</value>
   </webElementXpaths>
</WebElementEntity>
