import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl(GlobalVariable.URL)

WebUI.mouseOver(findTestObject('Service NSW/Page_Home  Service NSW/Input Find Locations'))

WebUI.setText(findTestObject('Service NSW/Page_Home  Service NSW/Input Find Locations'), FindLocation)

WebUI.click(findTestObject('Service NSW/Page_Home  Service NSW/Find Locations'))

WebUI.click(findTestObject('Service NSW/Page_Search  Service NSW/a_Find locations'))

WebUI.setText(findTestObject('Service NSW/Page_Find a Service NSW location  Service NSW/Input Search by suburb'), Suburb)

WebUI.click(findTestObject('Service NSW/Page_Find a Service NSW location  Service NSW/Search by suburb'))

WebUI.click(findTestObject('Service NSW/Page_Find a Service NSW location  Service NSW/Filter All'))

if (WebUI.verifyElementPresent(findTestObject('Service NSW/Page_Find a Service NSW location  Service NSW/a_Marrickville Service Centre'), 
    3, FailureHandling.OPTIONAL)) {
    WebUI.comment('Suburb found')

    WebUI.delay(2)

    WebUI.click(findTestObject('Service NSW/Page_Find a Service NSW location  Service NSW/a_Marrickville Service Centre'))

    WebUI.verifyElementPresent(findTestObject('Service NSW/Page_Marrickville Service Centre  Service NSW/div_Address'), 
        3)
} else {
    WebUI.comment('We couldn\'t find that suburb or postcode.')
}

