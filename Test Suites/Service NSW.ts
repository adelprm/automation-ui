<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Service NSW</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>29072acf-2a08-4516-96a5-d8a5cf194e5f</testSuiteGuid>
   <testCaseLink>
      <guid>583cf8c7-79b2-4037-9cf3-192edba5217c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Service NSW</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>e802b036-9b7f-43d1-9d7e-b00b496da7d3</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Service NSW (1)</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>e802b036-9b7f-43d1-9d7e-b00b496da7d3</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Suburb</value>
         <variableId>70930377-2654-4664-8ac8-88aab6fe0695</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>e802b036-9b7f-43d1-9d7e-b00b496da7d3</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>FindLocation</value>
         <variableId>a8f635d2-1357-4577-871c-7a1c3c1faf60</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
